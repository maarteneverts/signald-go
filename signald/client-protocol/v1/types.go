package v1

// JsonAddress is a signal user's contact information. a phone number, UUID or both
type JsonAddress struct {
	UUID   string `json:"uuid,omitempty"`
	Number string `json:"number"`
}

type JsonMessageRequestResponseMessage struct {
	Person  JsonAddress `json:"person"`
	GroupID string      `json:"groupId"`
	Type    string      `json:"type"`
}

type JsonReaction struct {
	Emoji               string      `json:"emoji"`
	Remove              bool        `json:"remove"`
	TargetAuthor        JsonAddress `json:"targetAuthor"`
	TargetSentTimestamp uint64      `json:"targetSentTimestamp"`
}

type JsonReadMessage struct {
	Sender    JsonAddress `json:"sender"`
	Timestamp uint64      `json:"timestamp"`
}

type JsonSendMessageResult struct {
	Address             JsonAddress `json:"address"`
	Success             Success     `json:"success"`
	NetworkFailure      bool        `json:"networkFailure"`
	UnregisteredFailure bool        `json:"unregisteredFailure"`
	IdentityFailure     string      `json:"identityFailure"`
}

type Success struct {
	Unidentified bool `json:"unidentified"`
	NeedsSync    bool `json:"needsSync"`
}

type RequestValidationFailure struct {
	ValidationResults []string `json:"validationResults"`
	Type              string   `json:"type"`
	Message           string   `json:"message"`
}
