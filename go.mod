module git.callpipe.com/finn/signald-go

go 1.14

require (
	github.com/mdp/qrterminal v1.0.1
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
)
